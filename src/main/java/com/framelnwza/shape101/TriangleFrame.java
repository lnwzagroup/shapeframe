/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.shape101;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author Gigabyte
 */
public class TriangleFrame {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Triangle");
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblBase = new JLabel("Base: ", JLabel.TRAILING);
        lblBase.setSize(50, 20);
        lblBase.setBackground(Color.red);
        lblBase.setOpaque(true);
        lblBase.setLocation(5, 5);
        frame.add(lblBase);
        
        JLabel lblheight = new JLabel("Height: ", JLabel.TRAILING);
        lblheight.setSize(50, 50);
        lblheight.setBackground(Color.red);
        lblheight.setOpaque(true);
        lblheight.setLocation(5, 5);
        frame.add(lblheight);
        
        JLabel lblSide = new JLabel("Side: ", JLabel.TRAILING);
        lblSide.setSize(50, 90);
        lblSide.setBackground(Color.red);
        lblSide.setOpaque(true);
        lblSide.setLocation(5, 5);
        frame.add(lblSide);

        JTextField txtbase = new JTextField();
        txtbase.setSize(50, 20);
        txtbase.setLocation(60, 5);
        frame.add(txtbase);
        
        JTextField txtheight = new JTextField();
        txtheight.setSize(50, 50);
        txtheight.setLocation(60, 5);
        frame.add(txtheight);
        
        JTextField txtside = new JTextField();
        txtside.setSize(50, 90);
        txtside.setLocation(60, 5);
        frame.add(txtside);

        JButton btn = new JButton("Calculate");
        btn.setSize(100, 20);
        btn.setLocation(120, 5);
        frame.add(btn);

        JLabel lblresult = new JLabel("Triangle Area:??? Triangle Perimeter:???");
        lblresult.setHorizontalAlignment(JLabel.CENTER);
        lblresult.setSize(300, 200);
        lblresult.setBackground(Color.cyan);
        lblresult.setOpaque(true);
        lblresult.setLocation(0, 50);
        frame.add(lblresult);

        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strbase = txtbase.getText();
                    String strheight = txtheight.getText();
                    String strSide = txtside.getText();
                    double Base = Double.parseDouble(strbase);
                    double Height = Double.parseDouble(strheight);
                    double Side = Double.parseDouble(strSide);
                    Triangle triangle = new Triangle(Base,Height);
                    lblresult.setText("Triangle Area= " + String.format("%.2f", triangle.calArea()) + " " + "Rectangle perimeter= " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception ex) {
                    System.out.println("Error!!!"+ex.getMessage());

                }
            }

        });
        frame.setVisible(true);
    }
}