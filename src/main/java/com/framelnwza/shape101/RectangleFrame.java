/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.shape101;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author Gigabyte
 */
public class RectangleFrame {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Rectangle");
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblwide = new JLabel("Wide: ", JLabel.TRAILING);
        lblwide.setSize(50, 20);
        lblwide.setBackground(Color.red);
        lblwide.setOpaque(true);
        lblwide.setLocation(5, 5);
        frame.add(lblwide);
        
        JLabel lblheight = new JLabel("height: ", JLabel.TRAILING);
        lblheight.setSize(50, 50);
        lblheight.setBackground(Color.red);
        lblheight.setOpaque(true);
        lblheight.setLocation(5, 5);
        frame.add(lblheight);

        JTextField txtwide = new JTextField();
        txtwide.setSize(50, 20);
        txtwide.setLocation(60, 5);
        frame.add(txtwide);
        
        JTextField txtheight = new JTextField();
        txtheight.setSize(50, 50);
        txtheight.setLocation(60, 5);
        frame.add(txtheight);

        JButton btn = new JButton("Calculate");
        btn.setSize(100, 20);
        btn.setLocation(120, 5);
        frame.add(btn);

        JLabel lblresult = new JLabel("Rectangle Area:??? Perimeter:???");
        lblresult.setHorizontalAlignment(JLabel.CENTER);
        lblresult.setSize(300, 50);
        lblresult.setBackground(Color.cyan);
        lblresult.setOpaque(true);
        lblresult.setLocation(0, 50);
        frame.add(lblresult);

        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strwide = txtwide.getText();
                    String strheight = txtheight.getText();
                    double w = Double.parseDouble(strwide);
                    double h = Double.parseDouble(strheight);
                    Rectangle rectangle = new Rectangle(w,h);
                    lblresult.setText("Rectangle Area= " + String.format("%.2f", rectangle.calArea()) + " " + "Rectangle perimeter= " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception ex) {
                    System.out.println("Error!!!"+ex.getMessage());

                }
            }

        });
        frame.setVisible(true);
    }
}