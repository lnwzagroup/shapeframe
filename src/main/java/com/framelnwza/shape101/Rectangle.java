/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.shape101;

/**
 *
 * @author Gigabyte
 */
public class Rectangle extends shape{
    private double w;
    private double h;

    public Rectangle(double w,double h) {
        super("Rectangle");
        this.h=h;
        this.w=w;
    }

    public void setW(double w) {
        this.w = w;
    }

    public void setH(double h) {
        this.h = h;
    }

    public double getW() {
        return w;
    }

    public double getH() {
        return h;
    }

    @Override
    public double calArea() {
        return w*h;
    }

    @Override
    public double calPerimeter() {
        return 2*(w+h);
    }
    
}
