/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.shape101;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author Gigabyte
 */
public class SquareFrame {
  public static void main(String[] args) {
        JFrame frame = new JFrame("Square");
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblside = new JLabel("Side: ", JLabel.TRAILING);
        lblside.setSize(50, 20);
        lblside.setBackground(Color.red);
        lblside.setOpaque(true);
        lblside.setLocation(5, 5);
        frame.add(lblside);
 
        JTextField txtside = new JTextField();
        txtside.setSize(50, 20);
        txtside.setLocation(60, 5);
        frame.add(txtside);
        

        JButton btn = new JButton("Calculate");
        btn.setSize(100, 20);
        btn.setLocation(120, 5);
        frame.add(btn);

        JLabel lblresult = new JLabel("Square Area:??? Square Perimeter:???");
        lblresult.setHorizontalAlignment(JLabel.CENTER);
        lblresult.setSize(300, 50);
        lblresult.setBackground(Color.cyan);
        lblresult.setOpaque(true);
        lblresult.setLocation(0, 50);
        frame.add(lblresult);

        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strside = txtside.getText();
                    double Side = Double.parseDouble(strside);
               
                    Square square = new Square(Side);
                    lblresult.setText("Square Area= " + String.format("%.2f", square.calArea()) + " " + "Square perimeter= " + String.format("%.2f", square.calPerimeter()));
                } catch (Exception ex) {
                    System.out.println("Error!!!"+ex.getMessage());

                }
            }

        });
        frame.setVisible(true);
    }
}
