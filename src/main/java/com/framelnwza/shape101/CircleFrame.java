/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.shape101;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author Gigabyte
 */
public class CircleFrame {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Circle");
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblRedius = new JLabel("radius: ", JLabel.TRAILING);
        lblRedius.setSize(50, 20);
        lblRedius.setBackground(Color.red);
        lblRedius.setOpaque(true);
        lblRedius.setLocation(5, 5);
        frame.add(lblRedius);

        JTextField txtredius = new JTextField();
        txtredius.setSize(50, 20);
        txtredius.setLocation(60, 5);
        frame.add(txtredius);

        JButton btn = new JButton("Calculate");
        btn.setSize(100, 20);
        btn.setLocation(120, 5);
        frame.add(btn);

        JLabel lblresult = new JLabel("Circle radius :??? area:??? Perimeter:???");
        lblresult.setHorizontalAlignment(JLabel.CENTER);
        lblresult.setSize(300, 50);
        lblresult.setBackground(Color.cyan);
        lblresult.setOpaque(true);
        lblresult.setLocation(0, 50);
        frame.add(lblresult);

        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strRedius = txtredius.getText();
                    double redius = Double.parseDouble(strRedius);
                    Circle circle = new Circle(redius);
                    lblresult.setText("Circle r = " + String.format("%.2f", circle.getRedius()) + " " + "Area= " + String.format("%.2f", circle.calArea()) + " " + "perimeter= " + String.format("%.2f", circle.calPerimeter()));
                } catch (Exception ex) {
                    System.out.println("Error!!!"+ex.getMessage());

                }
            }

        });
        frame.setVisible(true);
    }
}
