/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.shape101;

/**
 *
 * @author Gigabyte
 */
public class Triangle extends shape{
    private double Base;
    private double Height;
    private double Side;
    static final double B=0.5;

    public Triangle(double Base,double Height) {
        super("Triangle");
        this.Base=Base;
        this.Height=Height;
        this.Side=Side;
    }

    public double getBase() {
        return Base;
    }

    public void setBase(double Base) {
        this.Base = Base;
    }

    public double getHeight() {
        return Height;
    }

    public void setHeight(double Height) {
        this.Height = Height;
    }

    public double getSide() {
        return Side;
    }

    public void setSide(double Side) {
        this.Side = Side;
    }

    @Override
    public double calArea() {
        return Base*Height*B;
    }

    @Override
    public double calPerimeter() {
        return Base+Height+B;
    }
    
}
