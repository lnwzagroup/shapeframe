/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framelnwza.shape101;

/**
 *
 * @author Gigabyte
 */
public class Square extends shape{
    private double Square;
    

    public Square(double Square) {
        super("Square");
        this.Square=Square;
    }

    public double getSquare() {
        return Square;
    }

    public void setSquare(double Square) {
        this.Square = Square;
    }

    @Override
    public double calArea() {
        return Square*Square;
    }

    @Override
    public double calPerimeter() {
        return Square*4;
    }
    
}
